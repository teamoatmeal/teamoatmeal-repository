﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MVC_EF_Computerverwaltung.Migrations
{
    public partial class WithOsIpEtc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Computers_RoomID",
                table: "Computers");

            migrationBuilder.AddColumn<string>(
                name: "IPAddress",
                table: "Computers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MacAddress",
                table: "Computers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OS",
                table: "Computers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Computers_RoomID",
                table: "Computers",
                column: "RoomID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Computers_RoomID",
                table: "Computers");

            migrationBuilder.DropColumn(
                name: "IPAddress",
                table: "Computers");

            migrationBuilder.DropColumn(
                name: "MacAddress",
                table: "Computers");

            migrationBuilder.DropColumn(
                name: "OS",
                table: "Computers");

            migrationBuilder.CreateIndex(
                name: "IX_Computers_RoomID",
                table: "Computers",
                column: "RoomID",
                unique: true,
                filter: "[RoomID] IS NOT NULL");
        }
    }
}
