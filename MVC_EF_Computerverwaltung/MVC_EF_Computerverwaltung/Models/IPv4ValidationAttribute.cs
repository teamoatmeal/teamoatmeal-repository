﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MVC_EF_Computerverwaltung.Models
{
    public class IPv4ValidationAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            string pattern = @"^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9] ?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$";


            return Regex.IsMatch(value.ToString(), pattern);
        }


    }
}
