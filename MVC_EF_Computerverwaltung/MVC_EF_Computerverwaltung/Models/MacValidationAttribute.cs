﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MVC_EF_Computerverwaltung.Models
{
    public class MacValidationAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            string pattern = @"^[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}$";
            
            return Regex.IsMatch(value.ToString(), pattern);
        }
    }
}
