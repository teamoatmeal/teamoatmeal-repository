﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_EF_Computerverwaltung.Models
{
    public class ComputerOrgaContext :DbContext
    {
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Computer> Computers { get; set; }

        public ComputerOrgaContext(DbContextOptions options) : base(options)
        {
            

        }


    }
}
