﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_EF_Computerverwaltung.Models
{
    public class Computer
    {
        [Key]
        public int ComputerID { get; set; }
        [DisplayName("Computername")]
        public string ComputerName { get; set; }
        [DisplayName("Raum-ID")]
        public int? RoomID { get; set; }
        [DisplayName("Betriebssystem")]
        public string OS { get; set; }

        [DisplayName("IP-Adresse")]
        [IPv4Validation]
        public string IPAddress { get; set; }

        [DisplayName("Mac-Adresse")]
        [MacValidation]
        public string MacAddress { get; set; }

        public virtual Room Room { get; set; }

    }
}
