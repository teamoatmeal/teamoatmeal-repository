﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_EF_Computerverwaltung.Models
{
    public class Room
    {
        [Key]
        public int RoomID { get; set; }
        [DisplayName("Raum")]
        public string RoomName { get; set; }
        [DisplayName("Raumbeschreibung")]
        public string RoomDescription { get; set; }

        public virtual ICollection<Computer> Computer { get; set; }
    }
}
